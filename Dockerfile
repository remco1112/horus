FROM openjdk:8-alpine
COPY backend/build/libs/horus.jar /horus.jar
ENTRYPOINT ["java", "-jar", "horus.jar"]